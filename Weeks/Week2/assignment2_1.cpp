/* This assignment objective is to find the n-th fibonacci number*/
#include <iostream>
#include <unistd.h>
// #include "../../third-party/plf_nanotimer/plf_nanotimer.h"
#define RED "\033[31m"
#define GREEN "\033[32m"
#define CYAN "\033[36m"
#define RESET_COLOR "\033[0m"

unsigned long long int FibList(unsigned long long int n)
{
    unsigned long long int a = 0, b = 1;
    unsigned long long int c = 0;

    if (n == 0)
    {
        return 0;
    }
    for (int i = 2; i <= n; i++)
    {
        c = a + b;
        a = b;
        b = c;
    }
    return b;
}

int main()
{
    int number;
    // plf::nanotimer timer;

    std::cin >> number;
    unsigned long long int result = 0;
    //    timer.start();
    result = FibList(number);
    std::cout << result << std::endl;
    // std::cout << "In " << timer.get_elapsed_ms() << "ms." << std::endl;
    return 0;
}
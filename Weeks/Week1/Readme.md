# Week 1 Problems

## Listing 1 - Summation Problem

You must make a program that takes in two numbers and sum them, reporting the result in the console output.

## Listing 2 -  Maximum Parwise Product

Given a sequence on non-negative integer $0 \leq a_0, \dots,a_{n-1} \leq 10^5$, find the maximum pairwise product that is $\underset{0 \leq i \neq j \leq n-1}{max}a_i a_j$.

**Input format**: The first line of the input contains numbers $2 \leq n \leq 2 \centerdot 10^5.$. The next line contains $n$ non-negative integers $0 \leq a_0, \dots, a_{n-1} \leq 10^5$.

**Output format**: output a single number - the maximum pairwise product.

### Sample 1

Input:

```C++
3
1 2 3
```

Output:

```C++
6
```

### Sample 2

Input:

```C++
10
7 5 14 2 8 8 10 1 2 3
```

Output:

```C++
140
```

## Observations

* We cant figure out that to find the max parwise product of a sequence its to find the pair with the highest numbers of the sequence. So we don't actually need to compute all of them.

# DSA-Coursera-Spec

## Getting started

Just clone the repo, open it with vscode from the workspace file and everything should set itself up. Click `build [all]` and every single item should be built and be ready to be used.

## Levels of Algorithmic Design

* Naive Algorithm: from the definition of a problem to an algorithm. Simply slow;
* Algorithm by the way of Standard Tools: maybe faster, using standard techniques;
* Optimized Algorithm: improves existing solutions;
* Magic Algorithm: based on an unique insight.

## Grading System

### Stress Testing

#### Definition

In general it is a program that generates random tests in an infinite loop, and for each test, it launches your solution on this test and an alternative solution on te same test and compares the results. This alternative solution you also have to invent and implement yourself, but it is usually easy, because it can be any simple, slow, brute force solution, or just any other solution that you can come up with. The only requirement is that it should be significantly different from your main solution.

Then you just wait until you find a test on which your solutions differ. It one of them is correct and another is wrong, than it is a guarantee to happen, because there is some test for which your solution gives the wrong answer, and so they differ. If, however, both of your solutions are wrong, they are still almost guaranteed to have some test on which one of them gives wrong answers and another one gives right ones, because they're probably wrong in different places.

### Time and Memory limits

The time limits for C and C++ solutions is 1 second. The memory limit is 512Mb.

## Week 1 - Welcome

* [Readme for Week 1][def2]

## Week 2 - Why Study Algorithms?

* [Readme for Week 2][def3]

## Authors and acknowledgment

## Authors

* ![Linkedin Profile for Caio Guimarães](misc/icons/LinkedIN.svg) [Caio Guimarães][def]

## License

This project is under the Unlicense. For further readings refer to [The Unlicense License file](LICENSE)

## Project status

* WIP  - Work in Progress.

## TODO

* Integrate [plf::nanotimer][def4] as a git submodule and dependency to run time profiling on the assignments.

## References

[def]: https://www.linkedin.com/in/vilarcaio/
[def2]: Weeks/Week1/Readme.md
[def3]: Weeks/Week2/Readme.md
[def4]: https://github.com/mattreecebentley/plf_nanotimer

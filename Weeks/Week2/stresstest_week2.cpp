#include <iostream>
#include <vector>
#include "../../third-party/plf_nanotimer/plf_nanotimer.h"
#include <unistd.h>
#include <string>
#include <fstream>
#include <iterator>
#include <algorithm>
#define RED "\033[31m"
#define GREEN "\033[32m"
#define CYAN "\033[36m"
#define RESET_COLOR "\033[0m"
#define FILEPATH "/home/hellscoffe/Development/dsa-coursera-spec/misc/resources/test_database_fib.txt"

std::vector<unsigned long long int> loadDB(std::string filepath);

unsigned long long int FibList(unsigned long long int n)
{
    unsigned long long int a = 0, b = 1;
    unsigned long long int c = 0;

    if (n == 0)
    {
        return 0;
    }
    for (int i = 2; i <= n; i++)
    {
        c = a + b;
        a = b;
        b = c;
    }
    return b;
}

unsigned long long int FibListLastNum(unsigned long long int n)
{
    unsigned long long int a = 0, b = 1;
    unsigned long long int c = 0;

    if (n == 0)
    {
        return 0;
    }
    for (int i = 2; i <= n; i++)
    {
        c = a + b;
        a = b;
        b = c;
    }
    return b % 10;
}
int main()
{
    std::vector<unsigned long long int> test_db;
    std::vector<unsigned long long int>::iterator test_db_itr;
    test_db = loadDB(FILEPATH);
    if (test_db.empty())
    {
        return 0;
    }

    plf::nanotimer timer;
    unsigned long long int counter = 0;

    for (test_db_itr = test_db.begin(); test_db_itr != test_db.end(); test_db_itr++)
    {
        timer.start();
        unsigned long long int result = -1;
        result                        = FibList(counter);
        std::cout << "Inplace Calculation Test: "
                  << " in " << timer.get_elapsed_ns() << " ns..." << std::endl;
        if (result != *test_db_itr)
        {
            std::cout << RED << "[TEST] FAILED on " << counter
                      << "-th Fibonacci number!\n From Database: " << *test_db_itr << " | From solution: " << result
                      << RESET_COLOR << std::endl;
            break;
        }
        std::cout << GREEN << "[TEST] #" << counter << " OK! " << result << " = " << *test_db_itr << RESET_COLOR
                  << std::endl;
        counter++;
    }

    return 0;
}

std::vector<unsigned long long int> loadDB(std::string filepath)
{
    plf::nanotimer file_timer;
    file_timer.start();
    std::vector<unsigned long long int> database{}; // creates empty vector of int64_t
    std::fstream FileName;
    std::ifstream fileObj(filepath);
    unsigned long long tmpBuffer = 0;
    FileName.open(filepath, std::ios::in);
    if (!FileName)
    {
        std::cout << "Couldn't open file." << std::endl;
    }
    else
    {
        /*
        std::copy(
            std::istream_iterator<int64_t>(fileObj), std::istream_iterator<int64_t>(), std::back_inserter(database)
        );
        */
        while (FileName >> tmpBuffer)
        {
            database.push_back(tmpBuffer);
        }
        FileName.close();
    }

    std::cout << "File read in: " << file_timer.get_elapsed_ns() << " ns with " << database.size() << " entries/lines."
              << std::endl;
    return database;
}
# Week 2 Problems

## Learning Objectives

* Understand the type of problem that will be covered in this class;
* Recognize some problems for which sophisticated algorithms might not be necessary;
* Describe some artificial intelligence problems which go beyond the scope of this course.

## What we'll cover

* Focus on algorithms problems:
  * Clearly formulated;
  * Hard to do efficiently.

## Upcoming Problems

* Fibonacci Numbers;
* Greatest Common Divisors.
* These two are examples of why algorithms are important!
* They're straightforward algorithms: there is a problem statement and an algorithm that solves it, that is clear by simply interpreting the problem;
* Unfortunately the simple interpretable solutions are very slow;

## Fibonacci Numbers

$$
F_n = \begin{cases}
        0, & n=0, \\
        1, & n=1, \\
        F_{n-1} + F_{n-2}, & n \gt 1.
\end{cases}
$$

* Rapid growth lemma:
$F_n \geq 2^{n/2}$ for $n \geq 6.$
* Proof:
  By induction:
  Base Case: n = 6, 7 (by direct computation).
  Inductive Step:
  $$
  F_n = F_{n-1} + F_{n-2} \geq 2^{(n-1)/2} + 2^{(n-2)/2} \geq 2 \centerdot 2^{(n-2)/2} = 2^{n/2}.
  $$
* Which results in the formular for the nth of any Fibonacci Number:

$$
F_n = \frac{1}{\sqrt 5} \left( \left(\frac{1 + \sqrt 5}{2}\right)^n - \left(\frac{1 - \sqrt 5}{2}\right)^n\right)
$$

### Computing Fibonacci Numbers Naively

* Compute $F_n$:
  * Input: an integer $n \geq 0$.
  * Output: $F_n$.

### Computing in a more efficient way

FibList(n):

Create an array $F\left[0 \dots n \right]$

$F\left[ 0\right] \leftarrow 0 \\ F\left[ 1\right] \leftarrow 1$

for $i$ from $2$ to $n:$

$F\left[ i\right] \leftarrow F\left[ i-1\right] + F\left[i-2\right]$

return $F\left[n\right]$

## Greatest Common Dividers (GCDs) Problem

* Put fraction $\frac{a}{b}$ in simplest form;
* Divide numerator and denominador by $d$, to get $\frac{a/d}{b/d}$.
* We need a value $d$ that satisfies two properties:
  * $d$ had better divide both $a$ and $b$, since they're both integers;
  * $d$ must be as large as possible, so that we can reduce the fraction as much as we possibly can.

### Compute GCD

* Input: integers $a, b \geq 0$
* Output: gcd(a, b).

### Naive GCD Algorithm

$$
NaiveGCD(a, b)\\
\text{best} \leftarrow 0\\
\text{for} \ \ d \ \ \text{from}\ \  1 \ \ \text{to}\ \  a + b:\\
\text{if}\ \  d/a \ \ \text{and}\ \  d/b:
\ \ \text{best} \leftarrow d\\
return \ \ best
$$

### Efficient GCD Algorithm

* Key lemma: let $a^\prime$ be the remainder when $a$ is divided by $b$, then $gcd(a, b) = gcd(a^\prime, b) = gcd(b, a^\prime)$.
* Proof:
  * $a = a^\prime + bq$ for some $q$
  * $d$ divides $a$ and $b$ if and only if it divides $a^\prime$ and $b$.

#### Euclidian Algorithm

$$
if \ \ b = 0:\\
\ \ return \ \ a \\
a^\prime \leftarrow\ \ the \ \ remainder \ \ when \ \ a \ \ is \ \ divided \ \ by \ \ b \\
return \ \ EuclidGCD(b,a^\prime)\\
$$

* Each step reduces the size of numbers by about a factor of 2;
* Takes about $\log(ab)$ steps;
* GCDs of 100 digit numbers takes about 600 steps;
* Each step a single division.

## Computing Runtimes

The problem with computing runtimes is that thare multiple issues that basically arise from hardware properties and system design. All of these issues can multiply runtimes by a large constant. So measure runtime in a way that ignores constant multiples.

Consider asymptotic runtimes. How runtime scale with input size.

## Big - O Notation

$f(n) = \mathcal{O}(g(n))$ ($f$ is Big-$\mathcal{O}$ of $g$) or $f \preceq g$ if there exists constants $N$ and $c$ so that for all $n \geq N, f(n) \leq c \centerdot g(n)$. I.e. $f$ is bounded above by some constant multiple of $g$.

Examples:

$3n^2 + 5n + 2 = \mathcal{O}(n^2)$ since if $n \geq 1$, $3n^2 + 5n + 2n^2 \leq 3n^2 + 5n^2 + 2n^2  = 10n^2.$

### Disadvantages/Warnings

* Using Big-O loses important information about constant multiples;
* Big-O is only asymptotic.

## Common Big-O Notation Rules

* Multiplicative constants can be omitted:

$7n^3 = \mathcal{O}(n^3), \frac{n^2}{3} = \mathcal{O}(n^2)$.

* If you have two powers of $n$. The one with the larger exponent grows faster, so $n$ grows asymptotically slower than $\mathcal{O}(n^2)$. $\sqrt n$ grows slower than $n$, so it's $\mathcal{O}(n)$.

$n^a \prec n^b$  for  $0 < a < b:$

$n = \mathcal{O}(n^2), \sqrt n = \mathcal{O}(n)$

* If you have any polynomial and any exponential, the exponential always grows faster:

$n^a \prec b^n (a > 0, b > 1):$

$n^5 = \mathcal{O}(\sqrt 2^n), \ \ n^{100} = \mathcal{O}(1.1^n)$

* Any power of $\log n$ grows slower than any power of $n$:

$(\log n)^a \prec n^b (a, b > 0):$

$(\log n)^3 = \mathcal{O}(\sqrt n), \ \ n\log n = \mathcal{O}(n^2).$

* Smaller terms, such as sums/subtractions can be omitted:

$n^2 + n = \mathcal{O}(n^2), \ \ 2^n + n^9 = \mathcal{O}(2^n)$

## Log Quiz

The goal of this assignment is to practice with logarithms that appear frequently in the analysis of algorithms. Recall that $\log_a{n}$ is the power to which you need to raise $a$ in order to obtain $n$.

The main rules for working with logarithms are the following:

1. $\log_a{n^k} = k \log_a{n}$
2. $\log_a{n\cdot m} = \log_a{n} + \log_a{m}$
3. $n^{\log_a{b}} = b^{\log_a{n}}$
4. $\log_a{n} \cdot \log_b{a} = \log_b{n}$

### Question 1

Is it true that $(\log_5{n}^2 = 2 \cdot \log_5{n})$?

* Answer: No, since $(\log_5{n}^2$ is just $(\log_5{n}) \cdot (\log_5{n})$

### Question 2

$\log_2{n} \cdot \log_3{2} = \log_3{n}$

* Answer: Yes. Since, $\log_a{n} \cdot \log_b{a} = \log_b{n}$

### Question 3

$n^{\log_2{n}} = n$

* Answer: No. Since, $n^{\log_a{b}} = b^{\log_a{n}}$.

### Question 4

$\log_3{(2 \cdot n)} = \log_3{2} \cdot \log_3{n}$

* Answer: No, since $\log_a{n\cdot m} = \log_a{n} + \log_a{m}$. The correct is $\log_3​{2n} = \log_3{2} + \log_3{​n}$

### Question 5

$\log_10{n^2} = 2 \log_10{n}$

* Answer: Yes. Since, $\log_a{n^k} = k \log_a{n}$

### Question 6

$n^{\log_7{3}} = 7\log_3{n}$

* Answer: No. Since, $n^{\log_a{b}} = b^{\log_a{n}}$. Then the correct answer is $n^{\log_7{3}} = 3^{\log_7}{n}$

## Big-O Quiz

The goal of this assignment is to practice with big-O notation. Recall that we write f(n)=O(g(n))f(n)=O(g(n)) to express the fact that f(n)f(n) grows no faster than g(n)g(n): there exist constants NN and c>0c>0 so that for all n≥Nn≥N, f(n)≤c⋅g(n)f(n)≤c⋅g(n).

### Big-O 1

Is it true that $\log_2{n} = \mathcal{O}(n^2)$?

* Answer: No. A logarithmic function grows slower than a polynomial function.

### Big-O 2

$n log_2{n} = \mathcal{O}(n)$

* Answer: No. To compare these two functions, one first cancels $n$. What is left is $\log_2{n}$ versus $1$. Clearly, $\log_2{n}$ grows faster than $1$.

### Big-O 3

$n^2 = \mathcal{O}(n^3)$

* Answer: Yes. Since saying that a program is $\mathcal{O}(n^2)$ means that is at least this complex. Which the same as saying it is at least $\mathcal{O}(n^3)$.

### Big-O 4

$n = \mathcal{O}(\sqrt n)$.

* Answer: Wrong. Since, if a program is $\sqrt n = n^{1/2}$. Which means that is grows slower than $n = n^1$ as $\frac{1}{2} < 1.$

### Big-O 5

$5^{\log_2{n}} = \mathcal{O}(n^2)$

* Answer: No. Since, $a^{\log_b{c}} = c^{\log_b{a}}$ so $5^{\log_2{n}} = n^{\log_2{5}}$.

### Big-O 6

$n^5 = \mathcal{O}(2^{3 \log_2{n}})$

* Answer: No. Since, $2^{3 \log_2{n}} =  (2^{\log_2{n}})^3 = n^3$

### Big_O 7

$2^n = \mathcal{O}(2^{n+1})$

* Answer: Yes. Since, $2^{n+1} = 2 \cdot 2^n$, that is, $2^n$ and $2$ have the same growth rate and hence $2^n = \mathcal{\Theta}(2^{n+1})$

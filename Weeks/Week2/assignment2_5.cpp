
/* This assignment objective is to find the n-th fibonacci number N modulo M*/
#include <iostream>
#include <unistd.h>
// #include "../../third-party/plf_nanotimer/plf_nanotimer.h"
#define RED "\033[31m"
#define GREEN "\033[32m"
#define CYAN "\033[36m"
#define RESET_COLOR "\033[0m"

unsigned long long int FibListHuge(unsigned long long int N, long long int M)
{
    unsigned long long int a = 0, b = 1;
    unsigned long long int c = 0;

    if (N == 0)
    {
        return 0;
    }
    for (int i = 2; i <= N; i++)
    {
        c = a + b;
        a = b;
        b = c;
    }
    return b;
}

int main()
{
    unsigned long long int N;
    long long int M;
    // plf::nanotimer timer;

    std::cin >> N >> M;
    unsigned long long int result = 0;
    //    timer.start();
    result = FibListHuge(N, M);
    std::cout << result << std::endl;
    // std::cout << "In " << timer.get_elapsed_ms() << "ms." << std::endl;
    return 0;
}
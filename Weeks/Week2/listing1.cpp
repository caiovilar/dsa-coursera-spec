#include <iostream>
#include <vector>

int FibRecurs(int n)
{
    if (n <= 1)
    {
        return n;
    }
    else
    {
        return FibRecurs(n - 1) + FibRecurs(n - 2);
    }
}

int FibList(int n)
{
}

int NaiveGCD(int a, int b)
{
    int best = 0;
    for (int d = 1; d <= a + b; ++d)
    {
        if ((d / a) && (d / b))
        {
            best = d;
        }
    }
    return best;
}
int main()
{
    int number;
    std::cin >> number;
    std::cout << FibRecurs(number) << std::endl;
    return 0;
}

/* This assignment objective is to find the last digi of the n-th fibonacci number*/
#include <iostream>
#include <unistd.h>

unsigned long long int FibListLastNum(unsigned long long int n)
{
    unsigned long long int a = 0, b = 1;
    unsigned long long int c = 0;
    unsigned long long int lastnum[60];
    lastnum[0] = 0;
    lastnum[1] = 1;
    if (n == 0)
    {
        return 0;
    }
    for (int i = 2; i < 60; i++)
    {
        c          = a + b;
        a          = b;
        b          = c;
        lastnum[i] = b;
    }
    return lastnum[n % 60] % 10;
}
int main()
{
    unsigned long long int number;

    std::cin >> number;
    std::cout << FibListLastNum(number) << std::endl;

    return 0;
}
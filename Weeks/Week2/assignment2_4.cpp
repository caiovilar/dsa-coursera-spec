/* This assignment objective is to find the Least Common Multiple (LCM) of two numbers*/
#include <iostream>
#include <unistd.h>

int GCD(int a, int b)
{
    if (a == 0)
    {
        return b;
    }
    else
    {
        return GCD(b % a, a);
    }
}
unsigned long long int LCM(long int a, long int b)
{
    unsigned long long int numerator = a * b;
    int denominator                  = GCD(a, b);
    return numerator / denominator;
}

int main()
{
    int a, b;

    std::cin >> a >> b;
    std::cout << LCM(a, b) << std::endl;

    return 0;
}
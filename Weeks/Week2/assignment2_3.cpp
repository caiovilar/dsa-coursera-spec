/* This assignment objective is to find the Greatest Common Divisor (GCD) of two numbers*/
#include <iostream>
#include <unistd.h>

int GCD(int a, int b)
{
    if (a == 0)
    {
        return b;
    }
    else
    {
        return GCD(b % a, a);
    }
}
int main()
{
    int a, b;

    std::cin >> a >> b;
    std::cout << GCD(a, b) << std::endl;

    return 0;
}